import numpy as np
import matplotlib.pyplot as plt

recommendations = np.array(range(4, 21, 2))

sps_atkPopR  = np.array([2.87, 3.63, 4.46, 5.45, 6.43, 7.26, 7.79, 8.09, 8.70])
sps_atkSVD   = np.array([4.84, 6.58, 8.25, 9.15, 10.36, 12.25, 13.31, 14.07, 14.90])
sps_atkRNN   = np.array([2.50, 5.07, 6.20, 8.40, 8.55, 9.23, 9.53, 9.98, 10.44])

plt.figure()

plt.xlabel('number of recommendations (k)')
plt.ylabel('sps@k(%)')

plt.plot(recommendations, sps_atkRNN,   '-m', label = 'RNN system',     linewidth = 2)
plt.plot(recommendations, sps_atkSVD,   '-g', label = 'SVD system',     linewidth = 2)
plt.plot(recommendations, sps_atkPopR,  '-b', label = 'PopRank system', linewidth = 2)

plt.legend(loc = 'upper left')
#plt.ylim(2.0, 10.0)

plt.savefig('norecoms.pdf')
plt.close()

