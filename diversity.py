import numpy as np
import matplotlib.pyplot as plt

days = np.array(range(4, 64, 4))
days = np.insert(days, 3, 14)

divPopR  = np.array([61, 64, 65, 65, 66, 68, 68, 68, 69, 68, 68, 68, 75, 76, 76, 77])
divSVD   = np.array([330, 356, 378, 382, 388, 399, 408, 409, 419, 418, 418, 418, 421, 421, 428, 430])
divRNN   = np.array(days.shape[0] * [10])

plt.figure()

plt.xlabel('days from the training date')
plt.ylabel('diversity')

plt.plot(days, divRNN,   '-m', label = 'RNN system',     linewidth = 2)
plt.plot(days, divSVD,   '-g', label = 'SVD system',     linewidth = 2)
plt.plot(days, divPopR,  '-b', label = 'PopRank system', linewidth = 2)

plt.legend(loc = 'upper left')

plt.savefig('diversity.pdf')
plt.close()

