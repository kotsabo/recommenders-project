import numpy as np
import matplotlib.pyplot as plt

latent_factors = np.array(range(10, 110, 10))

sps_at4  = np.array([4.46, 4.31, 4.84, 4.99, 4.99, 4.92, 5.07, 4.84, 4.92, 4.99])
sps_at6  = np.array([5.60, 6.05, 6.58, 6.13, 6.58, 6.58, 6.20, 6.20, 6.51, 6.88])
sps_at8  = np.array([6.66, 7.26, 8.25, 7.87, 7.87, 7.49, 7.34, 6.88, 7.26, 7.41])
sps_at10 = np.array([7.72, 8.70, 9.15, 9.08, 8.93, 9.00, 8.70, 8.09, 8.02, 8.40])

plt.figure()

plt.xlabel('latent factors')
plt.ylabel('sps@k(%)')

plt.plot(latent_factors, sps_at4,  '-m', label = 'k = 4  recommendations', linewidth = 2)
plt.plot(latent_factors, sps_at6,  '-g', label = 'k = 6  recommendations', linewidth = 2)
plt.plot(latent_factors, sps_at8,  '-y', label = 'k = 8  recommendations', linewidth = 2)
plt.plot(latent_factors, sps_at10, '-b', label = 'k = 10 recommendations', linewidth = 2)

plt.legend(loc = 'lower right')
plt.ylim(2.0, 10.0)

plt.savefig('svd_factors_all.pdf')
plt.close()

