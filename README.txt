# Recurrent Neural Network Recommendation System

A recurrent neural network (RNN) in order to tackle the recommendation task.
Furthermore, there is implementation of two baseline recommendation systems; SVD and PopRank.

## Files

###
1.
popRank.ipynb -> PopRank baseline : you can change the number of recommendations k
sps_atk for validation and test subsets

2.
popRank_removeLowFreq.ipynb -> PopRank baseline but the low frequent movies have been removed from the sets

3.
svd.ipynb -> SVD baseline -> hyperparameters: number of recommendations (k) and number of latent factors
optimization of latent factors
sps_atk for validation and test subsets

4.
svd_removeLowFreq.ipynb -> SVD baseline but the low frequent movies have been removed from the sets

5.
rnn_implementation.ipynb -> RNN implementation recommendation system
grid search for optimization of the hyperparameters
sps_atk for validation and test subsets
diversity_atk
load filepath of different setups

6.
rnn_removeLowFreq.ipynb -> RNN system but the low frequent movies have been removed from the sets

7.
rnn_statistical_analysis.ipynb -> statistical analysis of the training labels

## .py files are for plotting different parameters

## ratings.csv, movies.csv are files with ratings and movies respectively taken from the MovieLens data sets
https://grouplens.org/datasets/movielens/

## Best Results folder contains the modles of the best RNN setups for sps@10

## Author

Ilias Kotsampougioukoglou