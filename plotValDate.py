import numpy as np
import matplotlib.pyplot as plt

days = np.array(range(4, 64, 4))
days = np.insert(days, 3, 14)
print(days)

validation_users = np.array([480, 910, 1209, 1322, 1424, 1669, 1953, 2249, 2571, 2756, 2974, 3220, 3472, 3735, 4002, 4206])

sps_at10RNN  = np.array([8.12, 8.02, 8.35, 8.40, 8.15, 8.51, 8.04, 7.47, 7.35, 7.26, 7.40, 7.20, 6.91, 6.96, 7.02, 7.01])
sps_at10SVD  = np.array([9.58, 9.45, 9.43, 9.15, 9.41, 9.71, 10.60, 10.63, 10.88, 11.25, 11.33, 11.46, 11.75, 11.65, 11.89, 11.82])
sps_at10PopR = np.array([4.17, 5.71, 5.38, 5.45, 5.62, 5.75, 6.76, 6.40, 6.99, 6.93, 7.40, 7.36, 7.09, 7.36, 7.57, 7.49])

# plot with various axes scales
plt.figure(1)
plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=0.2, hspace=0.5)

# RNN
plt.subplot(211)
plt.plot(days, sps_at10RNN,  '-m', label = 'RNN system',     linewidth = 2)
plt.plot(days, sps_at10SVD,  '-g', label = 'SVD system',     linewidth = 2)
plt.plot(days, sps_at10PopR, '-b', label = 'PopRank system', linewidth = 2)
plt.xlabel('days from the training date')
plt.ylabel('sps@10(%)')
plt.legend(loc = 'upper left', prop={'size': 6})
plt.grid(True)

plt.subplot(212)
plt.plot(validation_users, sps_at10RNN,  '-m', label = 'RNN system',     linewidth = 2)
plt.plot(validation_users, sps_at10SVD,  '-g', label = 'SVD system',     linewidth = 2)
plt.plot(validation_users, sps_at10PopR, '-b', label = 'PopRank system', linewidth = 2)
plt.xlabel('number of users')
plt.ylabel('sps@10(%)')
plt.legend(loc = 'upper left', prop={'size': 6})
plt.grid(True)

plt.savefig('changingDate.pdf')
plt.close()

